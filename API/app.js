const express = require('express');
const bodyParser = require('body-parser');
const graphqlHttp = require('express-graphql');
const { buildSchema } = require('graphql');
const app = express();

app.use(bodyParser.json());

app.use('/graphql', graphqlHttp({
    schema: buildSchema(`
    
    type rootQuery {
        orders: [String!]!
    }

    type rootMutation {
        createOrder(id: String): String
    }

    schema{
        query: rootQuery
        mutation: rootMutation
    }
    `),
    rootValue: {
        orders: () => {
            return ['23', '128','422'];
        },
        createEvent: (args) => {
            const idOrder = args.id;
            return idOrder
        }
    },
    graphiql: true
}));
    
app.listen(8080);